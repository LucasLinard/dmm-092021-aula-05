import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColorProvider with ChangeNotifier {
  MaterialColor _color = Colors.grey;

  MaterialColor get color => _color;

  set color(MaterialColor color) {
    _color = color;
    notifyListeners();
  }
}
