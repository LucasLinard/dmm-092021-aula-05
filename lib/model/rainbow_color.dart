import 'package:flutter/material.dart';

class RainbowColor {
  String name;
  MaterialColor color;
  RainbowColor({required this.name, required this.color});
}
