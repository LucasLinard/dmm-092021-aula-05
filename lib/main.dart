import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rainbow_colors/provider/color_theme.dart';

import 'model/rainbow_color.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  MaterialColor _materialColor = Colors.grey;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ColorProvider>(
            create: (context) => ColorProvider())
      ],
      child: Consumer<ColorProvider>(
        builder: (context, model, child) => MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: model.color,
          ),
          home: const MyHomePage(title: 'Flutter Demo Home Page'),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<RainbowColor> rainbowColors = [
    RainbowColor(name: "red", color: Colors.red),
    RainbowColor(name: "orange", color: Colors.orange),
    RainbowColor(name: "yellow", color: Colors.yellow),
    RainbowColor(name: "green", color: Colors.green),
    RainbowColor(name: "blue", color: Colors.blue),
    RainbowColor(name: "indigo", color: Colors.indigo),
    RainbowColor(name: "purple", color: Colors.purple),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "Escolha sua cor favorita",
            ),
            Text("Você escolheu a cor "),
            ElevatedButton(
                onPressed: () {
                  Provider.of<ColorProvider>(context, listen: false).color =
                      Colors.amber;
                },
                child: Text("Amber"))
          ],
        ),
      ),
    );
  }
}
